# 3- Cloud Run

Vamos a desplegar ahora nuestra aplicación en Cloud Run, y hacer pruebas y tests para ver qué configuraciones podemos usar.

## Desplegar imagen test

Usaremos una imagen pública primero para empezar a usar la plataforma:

1- Via la Consola

2- Usando el Cloud Shell. Este comando nos va a pedir un nombre de servicio (usamos cualquiera que no sea frontend/backend), región (preferiblemente usamos europe-west1), 
y si queremos invocaciones sin autorización **pulsamos y (permitir)**:

```
gcloud run deploy --image gcr.io/cloudrun/hello --platform managed
```

*Cuál es el dominio de nuestra aplicación? Porqué es random?* 

*Podemos desplegar sin necesidad a entrar los valores que se nos pide?*

## Desplegar imagen test con autenticación

Podemos hacerlo desde la consola o mediante el Cloud Shell, pero debemos seleccionar **NO permitir** invocaciones en el servicio sin autenticación.

Ejemplo:

```
gcloud run deploy hello-auth \
    --image gcr.io/cloudrun/hello \
    --platform managed \
    --region europe-west1 \
    --no-allow-unauthenticated
```

*Cómo podemos autenticarnos?*

## Desplegar nuestra nueva aplicación

Primero de todo vamos a lanzar los siguientes comandos para añadir la plataforma managed y la región europe-west1 cómo default:

```
gcloud config set run/platform managed
gcloud config set run/region europe-west1
```

Necesitamos la URL del servicio backend para añadirla cómo variable de entorno al servicio frontend, de modo que vamos a desplegar el backend primero:

```
gcloud run deploy backend \
    --image gcr.io/$PROJECT_ID/backend:v0 \
    --allow-unauthenticated \
    --set-env-vars=PROJECT_ID=$PROJECT_ID
```

Guardamos URL en variable de entorno:

`export URL=$(gcloud run services describe backend --region europe-west1 --platform managed --format json | jq -r .status.address.url)`

Ahora desplegamos frontend con las variables de entorno necesarias:

```
gcloud run deploy frontend \
    --image gcr.io/$PROJECT_ID/frontend:v0 \
    --allow-unauthenticated \
    --set-env-vars=PROJECT_ID=$PROJECT_ID,BACKEND_URL=$URL/process_image
```

Ya podemos testear nuestro servicio en la infraestructura de Cloud Run.

## Autoescalado

Vamos a modificar la concurrencia al servicio de frontend:

```
gcloud run services update frontend --concurrency=10
```

Vamos a usar una herramienta para estresar nuestra aplicación, por ejemplo [wrk](https://github.com/wg/wrk).

`wrk -c400 -t2 -d60s https://frontend-7cpxo2khia-ew.a.run.app/`


## Traffic split

Crear nuevo contenedor con un frontend modificado:

```
cd traffic-split
docker build -t gcr.io/antai-workshop-20200525/frontend:green .
docker push gcr.io/antai-workshop-20200525/frontend:green
```

Desplegar servicio, NO redirigir tráfico:

```
gcloud run deploy frontend \
    --image gcr.io/antai-workshop-20200525/frontend:green \
    --allow-unauthenticated \
    --no-traffic
```

Listamos las revisiones en el servicio:

```
gcloud run revisions list --service frontend
```

Hacer split de tráfico entre las dos últimas revisiones:

```
gcloud run services update-traffic frontend \
    --to-revisions=frontend-00001-tey=50,frontend-00002-gub=50
```
